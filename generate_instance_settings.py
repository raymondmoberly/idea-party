import os
from textwrap import dedent

def make_secret_key():
    """ Generates a random byte string """
    return os.urandom(64)

def generate_local_settings():
    key = make_secret_key()
    return dedent("""
	DEBUG = True
	SECRET_KEY = {}
	""".format(key))

print(generate_local_settings())
